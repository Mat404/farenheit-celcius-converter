#include <iostream>
#include <cmath>
using namespace std;

int main()
{

    int menu;
    cout << "Which are you converting from? (Fahrenheit: 1, Celsius: 2, Kelvin: 3)\n";
    cin >> menu;

    if (menu == 1)
    {
        int menu2;
        cout << "Which are you converting to? (Celsius: 1, Kelvin: 2)\n";
        cin >> menu2;

        if (menu2 == 1)
        {
            double temp;
            cout << "What temperature?\n";
            cin >> temp;
            cout << ((temp - 32) * (.555555)) << endl;
        }
        else if (menu2 == 2)
        {
            double temp;
            cout << "What temperature?\n";
            cin >> temp;
            cout << ((temp - 32) * (.555555) + 273.15) << endl;
        }
    }
    else if (menu == 2)
    {
        int menu2;
        cout << "Which are you converting to? (Fahrenheit: 1, Kelvin: 2)\n";
        cin >> menu2;

        if (menu2 == 1)
        {
            double temp;
            cout << "What temperature?\n";
            cin >> temp;
            cout << ((temp / .555555) + (32)) << endl;
        }
        else if (menu2 == 2)
        {
            double temp;
            cout << "What temperature?\n";
            cin >> temp;
            cout << (temp + 273.15) << endl;
        }
    }
    else if (menu == 3)
    {
        int menu2;
        cout << "Which are you converting to? (Fahrenheit: 1, Celsius: 2)\n";
        cin >> menu2;

        if (menu2 == 1)
        {
            double temp;
            cout << "What temperature?\n";
            cin >> temp;
            cout << ((temp - 273.15)/(.555555))+32 << endl;
        }
        else if (menu2 == 2)
        {
            double temp;
            cout << "What temperature?\n";
            cin >> temp;
            cout << temp - 273.15 << endl;
        }
    }
    else
    {
        cout << "Invalid Response\n";
    }
}